<?php

namespace SaiGlobal\Converter;

use PHPUnit\Framework\TestCase;

class ConverterTest extends TestCase
{

    public function conversionSuccessfulProvider()
    {
        // return data here
		//return [];
    }

    /**
     * @param $input
     * @param $output
     * @dataProvider conversionSuccessfulProvider
     */
    public function testRenderDisplay($input, $output)
    {
        $converter = new \SaiGlobal\Converter\Converter();
        $this->assertEquals($output, $converter->renderDisplay($input));
    }
}

