-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 13, 2019 at 10:09 PM
-- Server version: 8.0.15
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dataset`
--

-- --------------------------------------------------------

--
-- Table structure for table `record`
--

CREATE TABLE `record` (
  `record_id` int(11) NOT NULL,
  `record_category` varchar(10) NOT NULL,
  `record_sub_category` int(11) NOT NULL,
  `record_amount` int(11) NOT NULL,
  `record_created_at` datetime DEFAULT NULL,
  `record_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `record`
--

INSERT INTO `record` (`record_id`, `record_category`, `record_sub_category`, `record_amount`, `record_created_at`) VALUES
(1, 'A', 1, 53, '2019-03-12 14:00:00'),
(2, 'A', 4, 65, '2019-03-12 14:00:00'),
(3, 'A', 1, 58, '2019-03-12 14:07:09'),
(4, 'A', 5, 60, '2019-03-12 14:07:09'),
(5, 'B', 2, 10, '2019-03-12 14:07:09'),
(6, 'B', 5, 86, '2019-03-12 14:07:09'),
(7, 'B', 3, 75, '2019-03-12 14:07:09'),
(8, 'B', 2, 36, '2019-03-12 14:07:09'),
(9, 'C', 4, 20, '2019-03-12 14:07:09'),
(10, 'C', 3, 58, '2019-03-12 14:07:09'),
(11, 'C', 5, 44, '2019-03-12 14:07:09'),
(12, 'C', 4, 76, '2019-03-12 14:07:09'),
(13, 'A', 5, 66, '2019-03-12 14:07:09'),
(14, 'B', 3, 15, '2019-03-12 14:07:09'),
(15, 'C', 2, 87, '2019-03-12 14:07:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `record`
--
ALTER TABLE `record`
  ADD PRIMARY KEY (`record_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `record`
--
ALTER TABLE `record`
  MODIFY `record_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
