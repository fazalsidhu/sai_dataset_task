<?php
declare(strict_types = 1);
require __DIR__ . "/../src/SaiGlobal/Converter/Converter.php";
$converter = new \SaiGlobal\Converter\Converter();
(array) $records = $converter->fetchRecords();
if (count($records)) {
    (string) $status =  $converter->renderDisplay($records);
} else {
    echo "There is no record in DB.";
}
echo $status;