<?php
declare(strict_types = 1);
namespace SaiGlobal\Converter;

class Converter
{
    function fetchRecords()
    {
        $conn = mysqli_connect("localhost", "root", "", "dataset") or die("Couldn't connect");
        $sql = "SELECT `record_category`, `record_sub_category`, SUM(`record_amount`) as `record_amount` FROM `record`
        GROUP BY `record_category`, `record_sub_category` order by `record_category` asc";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        } else {
            return array();
        }
    }
    function renderDisplay(array $records): ? string
    {
        echo "<table border = '1' width = '50%' align='center'>";
        echo "<tr>";
        echo "<th> Category </th>";
        echo "<th>Subcategory</th>";
        echo "<th>Amount</th>";
        echo "</tr>";
        $previous_category = null;
        $category_record_amount = 0;
        foreach ($records as $row) {

            if ($row['record_category'] != $previous_category && $previous_category != null) {
                echo "<tr>";
                echo "<td colspan='2' align='center'>Total</td>";
                echo "<td>" . $category_record_amount . "</td>";
                echo "</tr>";
                $category_record_amount = 0;
            }
            echo "<tr>";
            echo "<td>" . $row['record_category'] . "</td>";
            echo "<td>" . $row['record_sub_category'] . "</td>";
            echo "<td>" . $row['record_amount'] . "</td>";
            echo "</tr>";
            $category_record_amount += $row['record_amount'];


            $previous_category = $row['record_category'];
        }
        echo "<tr>";
        echo "<td colspan='2' align='center'>Total</td>";
        echo "<td>" . $category_record_amount . "</td>";
        echo "</tr>";
        $category_record_amount = 0;
        echo "</table>";
		return '<br><br>Fetched Data from DB and presented as desired. ';
    }
}
